//
//  UploadViewController.swift
//  Report
//
//  Created by Mike Ponomaryov on 02.02.2020.
//  Copyright © 2020 illutex. All rights reserved.
//

import UIKit
//import NUP

class UploadViewController: UIViewController {
    
    enum Constants {
        static let uploadedTitle = "We've received your data!"
        static let uploadedMessage = "Thanks for sharing.\nWe'll contact you back soon..."
        static let uploadingFailedTitle = "Oops... Something went wrong"
        static let uploadingFailedMessage = "Please try again later"
        static let apiURL = "https://illutex.com/api/upload"
    }
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    var reportObject: ReportObject?
    private var isUploading = false
    
//MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        startUploading()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        cancelUploading()
    }
    
    @IBAction func cancelBtnTapped(_ sender: Any) {
        cancelUploading()
    }
}

//MARK: - Private
private extension UploadViewController {
    
    func startUploading() {
        guard let object = reportObject else {
            cancelUploading()
            return
        }
        
        if !isUploading {
            isUploading = true
            activityIndicatorView.startAnimating()
            print(object.description)
            
            let url = URL(string: Constants.apiURL)
            let request = NUDataRequest(url: url)
            request?.body = postRequest(from: object)
            request?.contentType = "application/x-www-form-urlencoded"
            request?.start(completion: { [weak self] (response, error) in
                guard let strongSelf = self else { return }
                DispatchQueue.main.async {
                    if let error = error {
                        strongSelf.uploadingFailed(error: error.localizedDescription)
                    } else if let data = response as? Data {
                        let responseText = String(data: data, encoding: .utf8)
                        print(responseText ?? "Done")
                        strongSelf.uploadingFinished()
                    }
                }
            })
        }
    }
    func cancelUploading() {
        isUploading = false
        activityIndicatorView.stopAnimating()
        navigationController?.popViewController(animated: true)
    }
    
    func uploadingFinished() {
        isUploading = false
        activityIndicatorView.stopAnimating()
        
        let alertController = alert(with: Constants.uploadedTitle, message: Constants.uploadedMessage)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func uploadingFailed(error: String?) {
        let message = error ?? Constants.uploadingFailedMessage
        let alertController = alert(with: Constants.uploadingFailedTitle, message: message)
        self.present(alertController, animated: true, completion: nil)
        cancelUploading()
    }
    
    func alert(with title: String, message: String) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.view.tintColor = UIColor.alertBgColor
        
        let okActionButton = UIAlertAction(title: "Ok", style: .default, handler: { action -> Void in
            self.navigationController?.popViewController(animated: true)
        })
        alertController.addAction(okActionButton)
        
        return alertController
    }
    
    func postRequest(from object: ReportObject) -> Data? {
        guard let postComposer = NUPOSTRequestComposer(), let comment = object.comment else { return nil }
        
        postComposer.addText(comment, forKey: "comment")
        for side in ImageSide.allCases {
            if let image = object.image(for: side), let data = image.jpegData(compressionQuality: 60.0) {
                let composerImage = NUPOSTRequestComposerImage()
                composerImage.name = side.toString() + ".jpeg"
                composerImage.type = NUPOSTRequestComposerImageTypeJPEG
                composerImage.data = data
                postComposer.add(composerImage, forKey: side.toString().replacingOccurrences(of: " ", with: "_"))
            }
        }
        
        return postComposer.body()
    }
}
