//
//  UIView+GlobalCoords.swift
//  Report
//
//  Created by Mike Ponomaryov on 03.02.2020.
//  Copyright © 2020 illutex. All rights reserved.
//

import UIKit

extension UIView {
    var globalPoint :CGPoint? {
        return superview?.convert(frame.origin, to: nil)
    }

    var globalFrame :CGRect? {
        return superview?.convert(frame, to: nil)
    }
}
