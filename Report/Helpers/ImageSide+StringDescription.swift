//
//  ImageSide+StringDescription.swift
//  Report
//
//  Created by Mike Ponomaryov on 03.02.2020.
//  Copyright © 2020 illutex. All rights reserved.
//

import Foundation

extension ImageSide {
    
    func toString() -> String {
        var description = ""
        switch self {
        case .left:
            description = "Left Side"
        case .right:
            description = "Right Side"
        case .front:
            description = "Front Side"
        case .rear:
            description = "Rear Side"
        }
        return description
    }
}
