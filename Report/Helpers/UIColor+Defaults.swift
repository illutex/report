//
//  UIColor+Defaults.swift
//  Report
//
//  Created by Mike Ponomaryov on 03.02.2020.
//  Copyright © 2020 illutex. All rights reserved.
//

import UIKit

extension UIColor {
    static let alertBgColor = UIColor(red: 0.0, green: 95.0 / 255.0, blue: 157.0 / 255.0, alpha: 1.0)
    static let cellBgColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.1)
    static let footerTextColor = UIColor.white
    static let footerPlaceholderTextColor = UIColor(white: 0.5, alpha: 1.0)
}
