//
//  ReportCollectionViewHeader.swift
//  Report
//
//  Created by Mike Ponomaryov on 01.02.2020.
//  Copyright © 2020 illutex. All rights reserved.
//

import UIKit

class ReportCollectionViewHeader: UICollectionReusableView {
    
    enum Constants {
        static let reuseID = "com.illutex.report.collection.header"
    }
    
    static func reuseIdentifier() -> String {
        return Constants.reuseID
    }
}
