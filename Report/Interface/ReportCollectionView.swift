//
//  ReportCollectionView.swift
//  Report
//
//  Created by Mike Ponomaryov on 1/29/20.
//  Copyright © 2020 illutex. All rights reserved.
//

import UIKit

protocol ReportCollectionViewDelegate: AnyObject {
    
    func collectionViewDidSelectItem(at indexPath: IndexPath)
}

class ReportCollectionView: UICollectionView {
    
    enum Constants {
        static let itemMargin: CGFloat = 20.0
        static let headerHeight: CGFloat = 100.0
        static let footerHeight: CGFloat = 300.0
    }
    
    @IBOutlet weak var nextButton: UIButton!
    weak var actionDelegate: ReportCollectionViewDelegate?
    
//MARK: - Lifecycle
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        delegate = self
        setupLayout()
    }
    
//MARK: - Private
    private func setupLayout() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let sideSize = ((bounds.width - Constants.itemMargin) / 2).rounded(.down)
        layout.sectionInset = UIEdgeInsets.zero
        layout.itemSize = CGSize(width: sideSize, height: sideSize)
        collectionViewLayout = layout
    }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension ReportCollectionView: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = bounds.size.width
        let sideSize = ((width - Constants.itemMargin) / 2).rounded(.down)
        return CGSize(width: sideSize, height: sideSize)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.itemMargin
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        actionDelegate?.collectionViewDidSelectItem(at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: bounds.size.width, height: Constants.headerHeight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: bounds.size.width, height: Constants.footerHeight)
    }
}
