//
//  ReportCollectionViewCell.swift
//  Report
//
//  Created by Mike Ponomaryov on 1/29/20.
//  Copyright © 2020 illutex. All rights reserved.
//

import UIKit

class ReportCollectionViewCell: UICollectionViewCell {
    
    enum Constants {
        static let reuseID = "com.illutex.report.collection.cell"
        static let defaultImage = UIImage(named: "ImageIcon")
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
//MARK: - Lifecycle
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    static func reuseIdentifier() -> String {
        return Constants.reuseID
    }
    
    func set(image: UIImage?, text: String) {
        imageView.image = image == nil ? Constants.defaultImage : image
        titleLabel.text = text
    }
    
//MARK: - Private
    private func setup() {
        backgroundColor = UIColor.cellBgColor
    }
}
