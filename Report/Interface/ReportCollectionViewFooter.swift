//
//  ReportCollectionViewFooter.swift
//  Report
//
//  Created by Mike Ponomaryov on 01.02.2020.
//  Copyright © 2020 illutex. All rights reserved.
//

import UIKit

protocol ReportCollectionViewFooterDelegate: AnyObject {
    
    func collectionViewFooterDidUpdateComment(_ comment: String)
    func collectionViewFooterDidTapNext()
}

class ReportCollectionViewFooter: UICollectionReusableView {
    
    enum Constants {
        static let reuseID = "com.illutex.report.collection.footer"
    }
    
    weak var delegate: ReportCollectionViewFooterDelegate?
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var nextButton: UIButton!
    public private(set) var comment = ""
    
    private lazy var placeholderText: String = {
        return self.commentTextView.text
    }()
    
//MARK: - Lifecycle
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setup()
    }

    static func reuseIdentifier() -> String {
        return Constants.reuseID
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        delegate?.collectionViewFooterDidTapNext()
    }
    
//MARK: - Private
    private func setup() {
        commentTextView.textColor = UIColor.footerPlaceholderTextColor
        commentTextView.delegate = self
    }
}

//MARK: - UITextViewDelegate
extension ReportCollectionViewFooter: UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if placeholderText == textView.text {
            textView.text = ""
            textView.textColor = UIColor.footerTextColor
        }
        
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if textView.text.isEmpty {
            textView.text = self.placeholderText
            textView.textColor = UIColor.footerPlaceholderTextColor
        }
        return true
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        comment = textView.text
        delegate?.collectionViewFooterDidUpdateComment(comment)
    }
}
