//
//  InfoViewController.swift
//  Report
//
//  Created by Mike Ponomaryov on 1/29/20.
//  Copyright © 2020 illutex. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    @IBOutlet weak var reportCollectionView: ReportCollectionView!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    
    lazy var reportObject = ReportObject()
    var pickedSide: ImageSide?
    private var tapRecognizer: UITapGestureRecognizer? = nil
    
    enum Constants {
        static let resultControllerSegue = "resultControllerSegue"
        static let permissionWarningTitle = "Warning"
        static let permissionWarningMessagePrefix = "You don't have permission to access "
        static let permissionWarningMessageCameraSuffix = "device's camera"
        static let permissionWarningMessageGallerySuffix = "photo gallery"
        static let incompleteDataTitle = "Additional data needed"
        static let incompleteDataMessage = "Please provide at least\none photo and a comment"
        static let maxWidth: CGFloat = 500.0
    }
    
//MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.view.backgroundColor = view.backgroundColor
        setupCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationWillEnterForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
        view.endEditing(true)
        teardownOutsideTapRecognizer()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let width = view.safeAreaLayoutGuide.layoutFrame.width
        let inset = width > Constants.maxWidth ? ((width - Constants.maxWidth) / 2).rounded(.down) : 20.0
        leadingConstraint.constant = inset
        trailingConstraint.constant = inset
        self.reportCollectionView.layoutIfNeeded()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        endEditing()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == Constants.resultControllerSegue {
            guard let uploadController = segue.destination as? UploadViewController else { return }
            uploadController.reportObject = reportObject
        }
    }
    
    @objc func applicationWillEnterForeground(_ notification: NSNotification) {
        view.endEditing(true)
        
        teardownOutsideTapRecognizer()
    }
    
    @objc func endEditing() {
        view.endEditing(true)
        
        teardownOutsideTapRecognizer()
    }
}

//MARK: - UIImagePickerControllerDelegate
extension InfoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage, let side = pickedSide {
            reportObject.setImage(image, for: side)
            reportCollectionView.reloadItems(at: [IndexPath(item: side.rawValue, section: 0)])
        }
        
        pickedSide = nil
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        pickedSide = nil
        picker.dismiss(animated: true, completion: nil)
    }
}

//MARK: - UICollectionViewDataSource, ReportCollectionViewDelegate
extension InfoViewController: UICollectionViewDataSource, ReportCollectionViewDelegate {
    
    func collectionViewDidSelectItem(at indexPath: IndexPath) {
        showPickerAlertSheet(for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ReportObject.sidesCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuseID = ReportCollectionViewCell.reuseIdentifier()
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseID, for: indexPath as IndexPath) as? ReportCollectionViewCell {
            let side = ImageSide.allCases[indexPath.item]
            let image = reportObject.image(for: side)
            cell.set(image: image, text: side.toString())
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showPickerAlertSheet(for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionView.elementKindSectionHeader {
            let reuseID = ReportCollectionViewHeader.reuseIdentifier()
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: reuseID, for: indexPath)
            return header
        }
        else if kind == UICollectionView.elementKindSectionFooter {
            let reuseID = ReportCollectionViewFooter.reuseIdentifier()
            if let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: reuseID, for: indexPath) as?
                ReportCollectionViewFooter {
                footer.delegate = self
                return footer
            }
            
        }
        return UICollectionReusableView()
    }
}

//MARK: - ReportCollectionViewFooterDelegate
extension InfoViewController: ReportCollectionViewFooterDelegate {
    
    func collectionViewFooterDidUpdateComment(_ comment: String) {
        reportObject.comment = comment
    }
    
    func collectionViewFooterDidTapNext() {
        if reportObject.isValid() {
            performSegue(withIdentifier: Constants.resultControllerSegue, sender: self)
        } else {
            showIncompleteDataAlertView()
        }
    }
}

//MARK: - Keyboard notifications
extension InfoViewController {
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard view.frame.origin.y == 0, let info = notification.userInfo,
            let currentFrame = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue,
            let targetFrame = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }

        var shiftedFrame = view.frame
        let kind = UICollectionView.elementKindSectionFooter
        let indexPath = IndexPath(item: 0, section: 0)
        if let footer = reportCollectionView.supplementaryView(forElementKind: kind, at: indexPath),
            let footerRect = footer.globalFrame,
            let navBar = navigationController?.navigationBar,
            let navBarScreenRect = navBar.globalFrame {
            
            if navBar.frame.height < navBarScreenRect.maxY {
                shiftedFrame.origin.y += targetFrame.origin.y - currentFrame.origin.y
            } else {
                shiftedFrame.origin.y -= footerRect.minY - navBar.frame.height
            }
        } else {
            shiftedFrame.origin.y += targetFrame.origin.y - currentFrame.origin.y
        }
        
        view.frame = shiftedFrame

        setupOutsideTapRecognizer()
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        var shiftedFrame = self.view.frame
        shiftedFrame.origin.y = 0
        view.frame = shiftedFrame
        
        teardownOutsideTapRecognizer()
    }
}

//MARK: - Private
private extension InfoViewController {
    
    func setupCollectionView() {
        let cellReuseID = ReportCollectionViewCell.reuseIdentifier()
        let cellNib = UINib(nibName: String(describing: ReportCollectionViewCell.self), bundle: .main)
        reportCollectionView.register(cellNib, forCellWithReuseIdentifier: cellReuseID)
        
        let headerReuseID = ReportCollectionViewHeader.reuseIdentifier()
        let headerNib = UINib(nibName: String(describing: ReportCollectionViewHeader.self), bundle: .main)
        reportCollectionView.register(headerNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:headerReuseID)
        
        let footerReuseID = ReportCollectionViewFooter.reuseIdentifier()
        let footerNib = UINib(nibName: String(describing: ReportCollectionViewFooter.self), bundle: .main)
        reportCollectionView.register(footerNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier:footerReuseID)
        
        reportCollectionView.actionDelegate = self
    }
    
    func setupOutsideTapRecognizer() {
        teardownOutsideTapRecognizer()
        
        let outsideTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(endEditing))
        outsideTapRecognizer.cancelsTouchesInView = false
        tapRecognizer = outsideTapRecognizer
        view.addGestureRecognizer(outsideTapRecognizer)
    }
    
    func teardownOutsideTapRecognizer() {
        if let outsideTapRecognizer = tapRecognizer {
            view.removeGestureRecognizer(outsideTapRecognizer)
            tapRecognizer = nil
        }
    }
}

//MARK: - UIAlertController's templates
private extension InfoViewController {
    
    func pickImage(from sourceType: UIImagePickerController.SourceType) {
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            let imagePicker = UIImagePickerController()
            imagePicker.modalPresentationStyle = .fullScreen
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = sourceType
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let title = Constants.permissionWarningTitle
            var message = Constants.permissionWarningMessagePrefix
            message += sourceType == .camera ? Constants.permissionWarningMessageCameraSuffix : Constants.permissionWarningMessageGallerySuffix
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.view.tintColor = UIColor.alertBgColor
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showPickerAlertSheet(for indexPath: IndexPath) {
        let side = ImageSide.allCases[indexPath.item]
        let actionSheetController = UIAlertController(title: "Upload Image", message: nil, preferredStyle: .actionSheet)
        actionSheetController.popoverPresentationController?.sourceView = reportCollectionView.cellForItem(at: indexPath)
        actionSheetController.view.tintColor = UIColor.alertBgColor
        
        let photoActionButton = UIAlertAction(title: "Take Photo", style: .default) { action -> Void in
            self.pickImage(from: .camera)
        }
        actionSheetController.addAction(photoActionButton)
        
        let galleryActionButton = UIAlertAction(title: "Choose From Gallery", style: .default) { action -> Void in
            self.pickImage(from: .photoLibrary)
        }
        actionSheetController.addAction(galleryActionButton)
        
        if reportObject.image(for: side) != nil {
            let clearActionButton = UIAlertAction(title: "Clear Photo", style: .default) { action -> Void in
                self.reportObject.setImage(nil, for: side)
                self.reportCollectionView.reloadItems(at: [indexPath])
            }
            actionSheetController.addAction(clearActionButton)
        }
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancelled")
        }
        actionSheetController.addAction(cancelActionButton)
        
        self.pickedSide = side
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func showIncompleteDataAlertView() {
        let alertController = UIAlertController(title: Constants.incompleteDataTitle, message: Constants.incompleteDataMessage, preferredStyle: .alert)
        alertController.view.tintColor = UIColor.alertBgColor
        
        let okActionButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(okActionButton)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
