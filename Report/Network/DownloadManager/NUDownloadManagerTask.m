//
//  NUDownloadManagerTask.m
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#import "NUDownloadManagerTask.h"

@interface NUDownloadManagerTask ()

@property (nonatomic, strong) NSNumber    *percent;
@property (nonatomic, strong) NSURL       *remoteURL;
@property (nonatomic, strong) NSURL       *tempFileURL;
@property (nonatomic, strong) NSString    *serverFilename;

@property (nonatomic, assign) NSUInteger  taskID;
@property (nonatomic, copy)   void (^downloadCompletion)(NSURL *localFileURL, NSError *error);

@end

@interface NUDownloadManagerTask (DownloadNotifications)

- (void)handleDidStartedDownloading:(NSNotification *)notification;
- (void)handleDidFinishedDownloading:(NSNotification *)notification;
- (void)handleDownloadingProgress:(NSNotification *)notification;
- (void)handleDidCompleteWithError:(NSNotification *)notification;

@end


@interface NUDownloadManagerTask (Memory)

- (void)hold;
- (void)unhold;

@end

static NSMutableArray <NUDownloadManagerTask *> *staticTasks = nil;

@implementation NUDownloadManagerTask

+ (instancetype)taskWithURL:(NSURL *)url
{
    id task = [[self alloc] initWithURL:url];
    return task;
}

- (instancetype)initWithURL:(NSURL *)url
{
    self = [super init];
    if (self)
    {
        self.tempFileURL = nil;
        self.downloadCompletion = nil;
        self.taskID = NSNotFound;
        self.percent = @(0.0f);
        self.remoteURL = url;
        self.priority = NUDownloadManagerTaskPriorityHigh;
        self.downloadingPath = nil;
        self.overwriteIfFileExists = YES;
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)startDownloadWithCompletion:(void (^)(NSURL *localFileURL, NSError *error))completion
{
    [self startDownloadWithCompletion:completion progressHandler:nil];
}

- (void)startDownloadWithCompletion:(void (^)(NSURL *localFileURL, NSError *error))completion progressHandler:(void (^)(NSNumber *progress))progressHandler
{
    self.progressHandler = progressHandler;

    [self hold];
    
    self.downloadCompletion = completion;
    self.percent = @(0.0f);
    [self _addObserversForDownloadManagersEvents];
    self.taskID = [[NUDownloadManager shared] downloadURL:self.remoteURL priority:self.priority];
}

- (void)cancelDownload
{
    self.downloadCompletion = nil;
    self.percent = @(0.0f);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NUDownloadManager shared] cancelTaskWithID:self.taskID];
    self.taskID = NSNotFound;
    
    [self unhold];
}

- (void)handleApplicationWillTerminate
{

}

#pragma mark - Private

- (void)_addObserversForDownloadManagersEvents
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDidStartedDownloading:)
                                                 name:DMDidStartDownloadingNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDidFinishedDownloading:)
                                                 name:DMDidFinishDownloadingNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDownloadingProgress:)
                                                 name:DMDownloadingProgressNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDidCompleteWithError:)
                                                 name:DMDidCompleteWithErrorNotification
                                               object:nil];
    
#if TARGET_OS_IOS || TARGET_OS_WATCH
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleApplicationWillTerminate)
                                                 name:UIApplicationWillTerminateNotification
                                               object:nil];
#else
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleApplicationWillTerminate)
                                                 name:NSApplicationWillTerminateNotification
                                               object:nil];
#endif

}

@end

@implementation NUDownloadManagerTask (DownloadNotifications)

- (void)handleDidStartedDownloading:(NSNotification *)notification
{
    NSURL *source = (NSURL *)notification.userInfo[DMSourceKey];
    NSUInteger identifier = (NSUInteger)[notification.userInfo[DMIdentifierKey] integerValue];
    if ([source.absoluteString isEqualToString:self.remoteURL.absoluteString] && identifier == self.taskID)
    {
        self.percent = @(0.0f);
        
        if (self.progressHandler)
        {
            self.progressHandler(self.percent);
        }
    }
}

- (void)handleDidFinishedDownloading:(NSNotification *)notification
{
    NSURL *source = (NSURL *)notification.userInfo[DMSourceKey];
    NSUInteger identifier = (NSUInteger)[notification.userInfo[DMIdentifierKey] integerValue];
    
    if ([source.absoluteString isEqualToString:self.remoteURL.absoluteString] && identifier == self.taskID)
    {
        NSString *destPath = self.downloadingPath.length ? self.downloadingPath : NSTemporaryDirectory();
        NSURL *fromLocation = (NSURL *)notification.userInfo[DMLocationKey];
        NSURL *toLocation = [NSURL fileURLWithPath:[destPath stringByAppendingPathComponent:notification.userInfo[DMSuggestedFileNameKey]]];
        NSError *error = nil;
        
        if (self.overwriteIfFileExists)
        {
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:toLocation.path];
            if (fileExists)
            {
                [[NSFileManager defaultManager] removeItemAtURL:toLocation error:&error];
                if (error)
                {
                    NSLog(@"Could not remove old file at path: %@\nError: %@", toLocation.path, error.localizedDescription);
                }
            }
        }
        
        [[NSFileManager defaultManager] moveItemAtURL:fromLocation
                                                toURL:toLocation
                                                error:&error];
        
        if (error)
        {
            // if error has occured returning filepath in temporary directory
            self.tempFileURL = fromLocation;
        }
        else
        {
            self.tempFileURL = toLocation;
        }
        
        self.serverFilename = notification.userInfo[DMSuggestedFileNameKey];
        self.percent = @(1.0f);
        self.taskID = NSNotFound;
        
        if (self.progressHandler)
        {
            self.progressHandler(self.percent);
        }
        
        if (self.downloadCompletion)
        {
            dispatch_async(dispatch_get_main_queue(), ^
            {
                self.downloadCompletion(self.tempFileURL, nil);
            });
        }
        
        [self unhold];
    }
}

- (void)handleDidCompleteWithError:(NSNotification *)notification
{
    NSURL *source = (NSURL *)notification.userInfo[DMSourceKey];
    NSUInteger identifier = (NSUInteger)[notification.userInfo[DMIdentifierKey] integerValue];
    NSError *error = notification.userInfo[DMErorKey];
    
    if ([source.absoluteString isEqualToString:self.remoteURL.absoluteString] && identifier == self.taskID)
    {
        self.taskID = NSNotFound;
        self.percent = [NSNumber numberWithDouble:0.0f];
        if (self.downloadCompletion)
        {
            dispatch_async(dispatch_get_main_queue(), ^
            {
                self.downloadCompletion(nil, error);
            });
        }
        
        [self unhold];
    }
}

- (void)handleDownloadingProgress:(NSNotification *)notification
{
    NSURL *source = (NSURL *)notification.userInfo[DMSourceKey];
    NSUInteger identifier = (NSUInteger)[notification.userInfo[DMIdentifierKey] integerValue];
    if ([source.absoluteString isEqualToString:self.remoteURL.absoluteString] && identifier == self.taskID)
    {
        NSNumber *percent = (NSNumber *)notification.userInfo[DMProgressKey];
        if (percent != nil)
        {
            self.percent = percent;
            
            if (self.progressHandler)
            {
                self.progressHandler(self.percent);
            }
        }
    }
}

@end

@implementation NUDownloadManagerTask (Memory)

- (void)hold
{
    @synchronized (staticTasks)
    {
        if (!staticTasks)
        {
            staticTasks = [NSMutableArray array];
        }
        [staticTasks addObject:self];
    }
}

- (void)unhold
{
    @synchronized (staticTasks)
    {
        [staticTasks removeObject:self];
        if (staticTasks.count == 0)
        {
            staticTasks = nil;
        }
    }
}

@end
