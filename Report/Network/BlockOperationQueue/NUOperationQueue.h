//
//  NUOperationQueue.h
//  NetworkUtilities
//
//  Created by Mike Ponomaryov on 07.08.2018.
//  Copyright © 2018 Mike Ponomaryov. All rights reserved.
//

#import "NUOperation.h"

@class NUOperationQueue;

typedef void (^NUOperationQueueCompletionHandler)(NUOperationQueue *operationQueue);

@interface NUOperationQueue : NSObject

@property (readonly)    BOOL                        isExecuting;
@property (readwrite)   BOOL                        simultaneousExecution;
@property (readonly)    NSArray <NUOperation *>     *operations;

- (void)addOperation:(NUOperation *)operation;
- (void)addOperations:(NSArray <NUOperation *> *)operations;
- (void)removeOperation:(NUOperation *)operation;
- (void)removeOperations:(NSArray <NUOperation *> *)operations;
- (void)startWithCompletionHandler:(NUOperationQueueCompletionHandler)completionHandler;
- (void)stop;

@end
