//
//  ReportObject.swift
//  Report
//
//  Created by Mike Ponomaryov on 1/30/20.
//  Copyright © 2020 illutex. All rights reserved.
//

import UIKit

enum ImageSide: Int, Hashable, CaseIterable {
    case left
    case right
    case front
    case rear
}

class ReportObject: CustomStringConvertible {
    
    private var images = [ImageSide: UIImage]()
    var comment: String? = nil
    
    func setImage(_ image: UIImage?, for side: ImageSide) {
        images[side] = image
    }
    
    func image(for side: ImageSide) -> UIImage? {
        return images[side]
    }
    
    func isValid() -> Bool {
        let requiredImages = 1 // or 'ReportObject.sidesCount()' if all images needed
        if let c = comment, !c.isEmpty, images.count >= requiredImages {
            return true
        }
        return false
    }
    
    static func sidesCount() -> Int {
        return ImageSide.allCases.count
    }
    
    var description: String {
        let pointer = Unmanaged.passUnretained(self).toOpaque()
        return "<\(type(of: self)): \(pointer)\n\timages count = \(images.count)\n\tcomment = \"\(comment ?? "NA")\">"
    }
}
